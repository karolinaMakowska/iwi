import threading

import matplotlib
from sklearn.datasets import make_blobs
from svm.video import Video
import tkinter as tk
from tkinter import *
from svm.binary_classes import SVM
from svm.kernel import Kernel
import numpy as np
from fileReader import FileReader
import matplotlib.pyplot as plt
import PIL
from PIL import ImageTk, ImageSequence
from PIL import Image

x = []
y = []
col = []
cl = []
sc = 'blue'
topLevel = None
g_ax = None
pathToFile = 'circles.txt'
fileFlag = True
window = Tk()

window.title("SVM - GUI")

window.geometry('460x680')

title = Label(window, text="Welcome to SVN tutorial", font=('Helvetica', 10, 'bold'))
split1 = Label(window,
               text="___________________________________________________________________________________________")
bC_lbl = Label(window, text="Basic Control:", font=('Helvetica', 9, 'bold', 'underline'))
split2 = Label(window,
               text="___________________________________________________________________________________________")
st_lbl = Label(window, text="Start Tutorial", font=('Helvetica', 10, 'bold'))
split3 = Label(window,
               text="___________________________________________________________________________________________")
setC_title_lbl = Label(window, text="Step 1: Set C", font=('Helvetica', 9, 'bold', 'underline'))
setC_lbl = Label(window, text="Set C:", font=('Helvetica', 9))
infoC_lbl = Label(window, text="Setup big C if you want to allow small mistake margin. \n "
                               "If you setup small C you will allow bigger mistake margin.", font=('Helvetica', 8))
gamma_lbl = Label(window, text="Gamma", font=('Helvetica', 9))
dimension_lbl = Label(window, text="Dimension in poly", font=('Helvetica', 9))
setC_txt = Entry(window, width=20)
gamma_txt = Entry(window, width=20)
dimension_txt = Entry(window, width=20)
sub_split1 = Label(window, text="...........................................................................",
                   font=('Arial', 15))
flie_title_lbl = Label(window, text="Step 2: Select \n input", font=('Helvetica', 9, 'bold', 'underline'))
flie_lbl = Label(window, text="File Input:", font=('Helvetica', 9))
infoflie__lbl = Label(window, text="Select a file input or click \n "
                                   "'Manual Point Selection' in order to pick points in GUI.", font=('Helvetica', 8))
readFromFile_txt = Entry(window, width=20)
sub_split2 = Label(window, text="...........................................................................",
                   font=('Arial', 15))
kernel_title_lbl = Label(window, text="Step 3: Run with \n specific kernel", font=('Helvetica', 9, 'bold', 'underline'))
kernel_info_lbl = Label(window, text="Run SVM with different kernels. \n "
                                     "You can control the behaviour using gamma and dimension parameters.",
                        font=('Helvetica', 8))

title.grid(column=0, row=0, columnspan=3)
split1.grid(column=0, row=1, columnspan=3)
bC_lbl.grid(column=0, row=2)
split2.grid(column=0, row=4, columnspan=3)
st_lbl.grid(column=0, row=6, columnspan=3)
split3.grid(column=0, row=7, columnspan=3)
setC_title_lbl.grid(column=0, row=8)
infoC_lbl.grid(column=0, row=9, columnspan=3)
setC_lbl.grid(column=0, row=10)
setC_txt.grid(column=1, row=10)
sub_split1.grid(column=0, row=11, columnspan=3)
flie_title_lbl.grid(column=0, row=12, pady=5)
infoflie__lbl.grid(column=0, row=13, columnspan=3)
flie_lbl.grid(column=0, row=14)
readFromFile_txt.grid(column=1, row=14)
sub_split2.grid(column=0, row=16, columnspan=3)
kernel_title_lbl.grid(column=0, row=18, padx=10)
kernel_info_lbl.grid(column=0, row=19, columnspan=3)
gamma_lbl.grid(column=0, row=20, columnspan=1)
dimension_lbl.grid(column=1, row=20, columnspan=1)
gamma_txt.grid(column=0, row=21, columnspan=1)
dimension_txt.grid(column=1, row=21, columnspan=1)

setC_txt.insert(INSERT, 0.5)
readFromFile_txt.insert(INSERT, pathToFile)
gamma_txt.insert(INSERT, 3.0)
dimension_txt.insert(INSERT, 3)


def example(num_samples=100, num_features=2, grid_size=200):
    # samples = np.array(np.random.normal(size=num_samples * num_features).reshape(num_samples, num_features))
    # labels = 2 * (samples.sum(axis=1) > 0) - 1.0
    samples, labels = make_blobs(n_samples=num_samples, centers=num_features, random_state=0, cluster_std=0.65)
    labels[labels == 0] = -1
    tmp = np.ones(len(samples))
    labels = tmp * labels

    # samples, labels = make_circles(num_samples, factor=0.2, noise=0.1)
    # labels[labels == 0] = -1
    # tmp = np.ones(len(samples))
    # labels = tmp * labels

    # with open("circles.txt", 'w') as f:
    #     for i in range(0, num_samples):
    #         f.write("{},{},{}\n".format(samples[i][0], samples[i][1], labels[i]))

    clf = SVM(Kernel.linear(), C=float(setC_txt.get()))
    clf.fit(samples, labels)
    print("final score {}".format(clf.score(samples, labels)))


def setFile():
    global pathToFile, fileFlag
    fileFlag = True
    pathToFile = str(readFromFile_txt.get())


def runFromFile(kernel):
    global window
    Video.cleanUp()
    imageRun()
    threading.Thread(target=workFile(kernel)).start()


def imageRun():
    global topLevel
    r = Toplevel(window)
    r.geometry("200x100")
    x = window.winfo_x()
    y = window.winfo_y()
    r.geometry("%dx%d+%d+%d" % (200, 100, x + 140, y + 280))
    topLevel = r
    l = Label(r, text="Please wait computation \n in progress ...", font=('Helvetica', 10, 'bold'), background="#D8D8D8")
    l.pack(side="bottom", fill="both", expand="yes")
    window.update()

def runFromDots(kernel):
    global window
    Video.cleanUp()
    imageRun()
    threading.Thread(target=workDots(kernel)).start()


def workDots(kernel):
    global x, y, cl, window, topLevel
    samples, labels = FileReader.dotMatrix(x, y, cl)
    clf = SVM(kernel, C=float(setC_txt.get()))
    clf.fit(samples, labels)
    print("final score {}".format(clf.score(samples, labels)))
    topLevel.destroy()
    window.update()
    Video.storePictures()
    Video.showVideo()


def workFile(kernel):
    global window, pathToFile, topLevel
    samples, labels = FileReader.readMatrix(pathToFile=pathToFile)
    clf = SVM(kernel, C=float(setC_txt.get()))
    clf.fit(samples, labels)
    print("final score {}".format(clf.score(samples, labels)))
    topLevel.destroy()
    window.update()
    Video.storePictures()
    Video.showVideo()

def clicked():
    Video.cleanUp()
    example()
    Video.storePictures()
    Video.showVideo()


def rerunVideo_Click():
    Video.showVideo()


def cleanUp_Click():
    Video.cleanUp()


def funNext(event):
    global sc
    if sc == 'blue':
        sc = 'red'
    else:
        sc = 'blue'


def onclick(event):
    global g_ax
    if event.inaxes == g_ax:
        x.append(event.xdata)
        y.append(event.ydata)
        if sc == 'red':
            col.append('red')
            cl.append(-1)
        else:
            col.append('blue')
            cl.append(1)

    # clear frame
    g_ax.clear()
    g_ax.autoscale(False)
    g_ax.set_xlim([-2, 2])
    g_ax.set_ylim([-2, 2])
    g_ax.scatter(x, y, color=col)  # inform matplotlib of the new data
    plt.draw()  # redraw


def plotDot():
    global fileFlag
    fileFlag = False
    fig, ax = plt.subplots()
    ax.autoscale(False)
    ax.scatter(x, y)
    global g_ax
    g_ax = ax
    axnext = plt.axes([0.70, 0.90, 0.2, 0.075])
    ax.set_xlim([-2, 2])
    ax.set_ylim([-2, 2])
    bnext = matplotlib.widgets.Button(axnext, 'Switch Class')
    bnext.on_clicked(funNext)
    fig.canvas.mpl_connect('button_press_event', onclick)
    plt.show()
    plt.draw()


def clearManuPoints():
    global x, y, col, cl, sc, g_ax, pathToFile, fileFlag
    x = []
    y = []
    col = []
    cl = []
    sc = 'blue'
    g_ax = None
    pathToFile = 'circles.txt'
    fileFlag = True


def kLinearExec():
    global fileFlag
    if fileFlag == True:
        runFromFile(Kernel.linear())
    else:
        runFromDots(Kernel.linear())


def kRbfExec():
    global fileFlag
    if fileFlag == True:
        runFromFile(Kernel.rbf(float(gamma_txt.get())))
    else:
        runFromDots(Kernel.rbf(float(gamma_txt.get())))


def kPolyExec():
    global fileFlag
    if fileFlag == True:
        runFromFile(Kernel._polykernel(dimension=float(dimension_txt.get()), gamma=float(gamma_txt.get())))
    else:
        runFromDots(Kernel._polykernel(dimension=float(dimension_txt.get()), gamma=float(gamma_txt.get())))


def kQuaExec():
    global fileFlag
    if fileFlag == True:
        runFromFile(Kernel.quadratic(gamma=float(gamma_txt.get())))
    else:
        runFromDots(Kernel.quadratic(gamma=float(gamma_txt.get())))


btn = Button(window, text="Set File", command=setFile, width=10)
btn.grid(column=2, row=14, padx=20, pady=10)

btn_clickPoints = Button(window, text="Manual Point Selection", command=plotDot, width=20)
btn_clearPoints = Button(window, text="Clear Points", command=clearManuPoints)
btn_clickPoints.grid(column=0, row=15, padx=20, pady=10, columnspan=2)
btn_clearPoints.grid(column=2, row=15, padx=20, pady=10)

rerunVideo_btn = Button(window, text="Rerun Video", command=rerunVideo_Click, width=15)
cleanUp_Click_btn = Button(window, text="Clean Pictures", command=cleanUp_Click, width=15)
runSample_Click_btn = Button(window, text="Run Example", command=clicked, width=15)

btn_kernelLinear = Button(window, text="Linear Kernel", command=kLinearExec, width=15)
btn_kernelRbf = Button(window, text="RBF Kernel", command=kRbfExec, width=15)
btn_kernelPoly = Button(window, text="Poly Kernel", command=kPolyExec, width=15)
btn_kernelQua = Button(window, text="Quadratic Kernel", command=kQuaExec, width=15)

runSample_Click_btn.grid(column=0, row=3, padx=10, pady=10)
rerunVideo_btn.grid(column=1, row=3, padx=10, pady=10)
cleanUp_Click_btn.grid(column=2, row=3, padx=10, pady=10)

btn_kernelLinear.grid(column=1, row=22, padx=10, pady=10)
btn_kernelRbf.grid(column=0, row=23, padx=10, pady=10)
btn_kernelPoly.grid(column=1, row=23, padx=10, pady=10)
btn_kernelQua.grid(column=2, row=23, padx=10, pady=10)

window.mainloop()
