import cv2
import os
import shutil


class Video:
    @staticmethod
    def storePictures():
        image_folder = 'picOutput'
        video_name = 'video.avi'

        images = [img for img in os.listdir(image_folder) if img.endswith(".png")]
        frame = cv2.imread(os.path.join(image_folder, images[0]))
        height, width, layers = frame.shape

        video = cv2.VideoWriter(video_name, 0, 1, (width, height))

        for image in images:
            video.write(cv2.imread(os.path.join(image_folder, image)))

        cv2.destroyAllWindows()
        video.release()

    @staticmethod
    def showVideo():
        cap = cv2.VideoCapture('video.avi')

        # Check if camera opened successfully
        if (cap.isOpened() == False):
            print("Error opening video  file")
        while (cap.isOpened()):
            ret, frame = cap.read()
            if ret == True:
                cv2.imshow('Frame', frame)
                if cv2.waitKey(800) & 0xFF == ord('q'):
                    break
            else:
                break
        #cap.release()
        #cv2.destroyAllWindows()

    @staticmethod
    def cleanUp():
        if os.path.exists('picOutput'):
            shutil.rmtree('picOutput')
        if not os.path.exists('picOutput'):
            os.makedirs('picOutput', mode=0o777)
