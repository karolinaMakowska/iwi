import numpy as np


# Collection of usual kernels
class Kernel(object):
    name = ''

    @staticmethod
    def linear():
        Kernel.name = 'linear'

        def f(x, y):
            return np.inner(x, y)

        return f

    @staticmethod
    def rbf(gamma):
        Kernel.name = 'rbf'

        def f(x, y):
            exponent = - gamma * np.linalg.norm(x - y) ** 2
            return np.exp(exponent)

        return f

    @staticmethod
    def _polykernel(dimension, offset=0.0, gamma=1.0):
        Kernel.name = 'poly'

        def f(x, y):
            return (gamma * (offset + np.dot(x, y))) ** dimension

        return f

    @staticmethod
    def quadratic(offset=0.0, gamma=1.0):
        Kernel.name = 'quadratic'

        def f(x, y):
            return (gamma * (offset + np.dot(x, y))) ** 2

        return f
