import matplotlib.pyplot as plt
from matplotlib.widgets import Button

x = []
y = []
col = []
cl = []
sc = 'blue'


def funNext(event):
     global sc
     if sc == 'blue':
        sc ='red'
     else:
        sc ='blue'


def onclick(event):
    if event.inaxes == ax:
         x.append(event.xdata)
         y.append(event.ydata)
         if sc == 'red':
            col.append('red')
            cl.append(-1)
         else:
            col.append('blue')
            cl.append(1)

    #clear frame
    ax.clear()
    ax.scatter(x, y, color=col) #inform matplotlib of the new data
    plt.draw() #redraw

fig,ax =  plt.subplots()
ax.scatter(x, y)
axnext = plt.axes([0.70, 0.90, 0.2, 0.075])
bnext = Button(axnext, 'Switch Class')
bnext.on_clicked(funNext)
fig.canvas.mpl_connect('button_press_event', onclick)
plt.show()
plt.draw()