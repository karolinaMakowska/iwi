from datetime import datetime

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import itertools
from svm.kernel import Kernel
import math


class Plotter:

    @staticmethod
    def plot(predictor, X, y, grid_size, current_score, current_iteration):
        x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
        y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
        xx, yy = np.meshgrid(np.linspace(x_min, x_max, grid_size),
                             np.linspace(y_min, y_max, grid_size),
                             indexing='ij')
        flatten = lambda m: np.array(m).reshape(-1, )

        result = []
        for (i, j) in itertools.product(range(grid_size), range(grid_size)):
            point = np.array([xx[i, j], yy[i, j]]).reshape(1, 2)
            result.append(predictor.predict(point))

        Z = np.array(result).reshape(xx.shape)
        plt.clf()

        plt.contourf(xx, yy, Z,
                     cmap=cm.Paired,
                     levels=[-0.001, 0.001],
                     extend='both',
                     alpha=0.4)

        plt.scatter(flatten(X[:, 0]), flatten(X[:, 1]), c=flatten(y), cmap=cm.Paired, edgecolors='k')

        w = predictor.w
        margin = (2 / np.sqrt(w[0] * w[0] + w[1] * w[1])) / 2

        if Kernel.name is 'linear':
            a = -w[0] / w[1]
            xx = np.linspace(-10, 10)
            yy = a * xx - predictor.intercept_ / w[1]
            yy_down = yy - np.sqrt(1 + a ** 2) * margin
            yy_up = yy + np.sqrt(1 + a ** 2) * margin
            plt.plot(xx, yy, 'k-')
            plt.plot(xx, yy_down, 'k--')
            plt.plot(xx, yy_up, 'k--')
        else:
            plt.contour(xx, yy, Z, colors='k', linewidths=0.9)

        plt.xlim(x_min, x_max)
        plt.ylim(y_min, y_max)
        txt = "Iteration {}   Score {}%   Margin = {:.3f}".format(current_iteration, current_score * 100, 2 * margin)
        plt.figtext(0.5, 0.01, txt, wrap=True, horizontalalignment='center', fontsize=12)
        datetime.now().time()
        plt.savefig(".\\picOutput\\" + str(datetime.now().strftime("%b-%d-%Y")) + str(
            datetime.now().time().strftime("%H-%M-%S")) + ".png")
