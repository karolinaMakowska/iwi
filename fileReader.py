import numpy as np
import os


class FileReader:
    @staticmethod
    def readMatrix(pathToFile='separable_close.txt'):
        ptf = pathToFile
        if ptf == "":
            ptf = 'samples.txt'
        else:
            ptf=os.path.normpath(ptf)

        fin = open(ptf, 'r')
        a = []
        text = []
        numLines = 0
        for line in fin.readlines():
            a.append([float(x) for x in line.split(',')])
            numLines += 1
        fin.close()
        print(a)
        text = np.array(a)
        text.reshape((numLines, 3))
        return text[:numLines, :2], text[:numLines, 2]

    @staticmethod
    def dotMatrix(x, y, z):
        numlines = np.size(x, 0)
        lines=0
        a = []
        for i in range(numlines):
            a.append([float(x[i]), float(y[i]), float(z[i])])
            lines += 1
        text = np.array(a)
        text.reshape((lines, 3))
        print(text)
        return text[:lines, :2], text[:lines, 2]
